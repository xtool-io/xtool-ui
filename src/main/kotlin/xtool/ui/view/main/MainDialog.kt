package xtool.ui.view.main

import org.springframework.stereotype.Component
import xtool.core.plugin.ui.PluginView
import xtool.ui.core.PluginTableModel
import xtool.ui.core.Plugins
import java.awt.BorderLayout
import java.awt.ComponentOrientation
import java.awt.Dimension
import java.awt.event.ActionEvent
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import javax.swing.*
import javax.swing.event.ListSelectionEvent
import javax.swing.event.ListSelectionListener

/**
 * Classe com a View principal do sistema.
 */
@Component
class MainDialog(val plugins: Plugins) : ListSelectionListener {

    val jFrame = JFrame()
    val jToolBar = JToolBar()
    val jTable = JTable(PluginTableModel(plugins))
    val btRunPlugin = JButton("Run")
    val btHelpPlugin = JButton("Help")

    fun showUI() {
        jFrame.layout = BorderLayout()
        jFrame.title = "Xtool"

        // TOOLBAR
        jToolBar.isFloatable = false
        jToolBar.isRollover = true
        jToolBar.componentOrientation = ComponentOrientation.LEFT_TO_RIGHT
        btRunPlugin.isEnabled = false
        btHelpPlugin.isEnabled = false
        jToolBar.add(btRunPlugin)
        jToolBar.add(btHelpPlugin)
        jFrame.add(jToolBar, BorderLayout.PAGE_START)

        // BUTTON
        btRunPlugin.addActionListener(this.onClickBtRunPlugin())

        // PLUGIN TABLE
        jTable.selectionModel.addListSelectionListener(this)
        val jScrollPane = JScrollPane(jTable)
        jFrame.contentPane.add(jScrollPane, BorderLayout.CENTER)

        jFrame.size = Dimension(600, 480)
        jFrame.setLocationRelativeTo(null)
        jFrame.isResizable = false
        jFrame.defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE
        jFrame.isVisible = true
    }

    /**
     * Listener da seleção de linha da tabela de plugins.
     */
    override fun valueChanged(e: ListSelectionEvent?) {
        btRunPlugin.isEnabled = true
        btHelpPlugin.isEnabled = true
    }

    /**
     * Listener do click no button: Run
     */
    fun onClickBtRunPlugin(): (ActionEvent) -> Unit {
        return {
            val pluginName = jTable.model.getValueAt(jTable.selectedRow, 0) as String;
            val pluginView = PluginView(plugins[pluginName]!!)
            pluginView.isVisible = true
        }
    }

    fun onCloseWindowListener(): WindowAdapter {
        return object : WindowAdapter() {
            override fun windowClosing(e: WindowEvent) {
                System.exit(0)
            }
        }
    }
}
