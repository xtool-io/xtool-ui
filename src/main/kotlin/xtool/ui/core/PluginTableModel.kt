package xtool.ui.core

import javax.swing.table.AbstractTableModel

class PluginTableModel(private val plugins: Plugins) : AbstractTableModel() {

    private val columnNames = arrayOf("Nome do plugin", "Descrição")

    private val columnsType = arrayOf(String.javaClass, String.javaClass)

    override fun getRowCount() = plugins.list.size

    override fun getColumnCount() = columnNames.size

    override fun getColumnName(column: Int) = columnNames[column]

    override fun getColumnClass(columnIndex: Int) = columnsType[columnIndex]

    override fun getValueAt(rowIndex: Int, columnIndex: Int) = when (columnIndex) {
        0 -> plugins.list[rowIndex].name
        1 -> plugins.list[rowIndex].description
        else -> ""
    }

}
