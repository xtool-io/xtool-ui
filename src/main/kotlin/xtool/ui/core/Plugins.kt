package xtool.ui.core

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import xtool.core.plugin.XtoolPlugin

/**
 * Classe que gerencia os plugins do classpath.
 */
@Component
class Plugins @Autowired(required = false) constructor(val plugins: List<out XtoolPlugin>) {
    var list: List<XtoolPlugin> = plugins

    operator fun get(s: String): XtoolPlugin? {
        return this.plugins.find { it.name == s }
    }
}
