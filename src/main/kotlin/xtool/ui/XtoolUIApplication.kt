package xtool.ui

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.core.io.ClassPathResource
import xtool.ui.view.main.MainDialog
import java.awt.Font
import java.util.*
import javax.swing.JDialog
import javax.swing.JFrame
import javax.swing.UIManager
import javax.swing.plaf.FontUIResource


@SpringBootApplication
class XtoolUIApplication(val mainDialog: MainDialog) : CommandLineRunner {
    override fun run(vararg args: String?) {
        mainDialog.showUI()
    }
}

fun main(args: Array<String>) {
    JFrame.setDefaultLookAndFeelDecorated(true)
    JDialog.setDefaultLookAndFeelDecorated(true)
    updateUIFont()
    val builder = SpringApplicationBuilder(XtoolUIApplication::class.java)
    builder.headless(false).run(*args)
}

fun updateUIFont() {
    val resource = ClassPathResource("/font/Roboto-Regular.ttf")
    var font = Font.createFont(Font.TRUETYPE_FONT, resource.inputStream)
    val keys: Enumeration<*> = UIManager.getDefaults().keys()
    while (keys.hasMoreElements()) {
        val key = keys.nextElement()
        val value = UIManager.get(key)
        if (value is FontUIResource) {
            val fontIUResource = FontUIResource(font.deriveFont(14f))
            UIManager.put(key, fontIUResource)
        }
    }
}
