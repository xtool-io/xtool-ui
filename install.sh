#!/bin/bash

echo -e ""
echo -e "Installing xtool.................................................."

export XTOOL_HOME=~/.xtool
xtool_bash_profile="${HOME}/.bash_profile"
xtool_profile="${HOME}/.profile"
xtool_bashrc="${HOME}/.bashrc"
xtool_zshrc="${HOME}/.zshrc"
xtool_init_snippet=$( cat << EOF
export XTOOL_HOME=~/.xtool
export PATH="\$PATH:\$XTOOL_HOME/bin"
EOF
)

cygwin=false;
darwin=false;
solaris=false;
freebsd=false;
case "$(uname)" in
    CYGWIN*)
        cygwin=true
        ;;
    Darwin*)
        darwin=true
        ;;
    SunOS*)
        solaris=true
        ;;
    FreeBSD*)
        freebsd=true
esac


mkdir -p $XTOOL_HOME/bin
mkdir -p $XTOOL_HOME/plugins

cp xtool $XTOOL_HOME/bin/xtool
cp target/xtool-ui.jar $XTOOL_HOME/xtool-ui.jar
cp src/main/resources/application.properties $XTOOL_HOME/application.properties

if [[ $darwin == true ]]; then
  touch "$xtool_bash_profile"
  echo "Attempt update of login bash profile on OSX..."
  if [[ -z $(grep 'XTOOL_HOME' "$xtool_bash_profile") ]]; then
    echo -e "\n$xtool_init_snippet" >> "$xtool_bash_profile"
    echo "Added xtool init snippet to $xtool_bash_profile"
  fi
else
  echo "Attempt update of interactive bash profile on regular UNIX..."
  touch "${xtool_bashrc}"
  if [[ -z $(grep 'XTOOL_HOME' "$xtool_bashrc") ]]; then
      echo -e "\n$xtool_init_snippet" >> "$xtool_bashrc"
      echo "Added xtool init snippet to $xtool_bashrc"
  fi
fi

echo "Attempt update of zsh profile..."
touch "$xtool_zshrc"
if [[ -z $(grep 'XTOOL_HOME' "$xtool_zshrc") ]]; then
    echo -e "\n$xtool_init_snippet" >> "$xtool_zshrc"
    echo "Updated existing ${xtool_zshrc}"
fi

echo -e "Xtool installed."
echo -e ""
