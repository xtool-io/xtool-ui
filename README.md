# XTOOL-UI

## Pré-requisitos

Para a correta instalação da ferramenta é necessário os seguintes pacotes:

- Java 8
- Git 
- Maven

## Instalação

Para realizar a instalação da ferramenta `xtool` é necessário executar os seguintes comandos no terminal:
```shell script
$ git clone git@gitlab.com:xtool-io/xtool-ui.git
$ cd xtool-ui
$ mvn clean package
```

Em seguinda, sair e entrar no terminal e digitar o comando:

```shell script
$ xtool
```
